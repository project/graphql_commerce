<?php

declare(strict_types=1);

namespace Drupal\Tests\graphql_commerce\Kernel;

use Drupal\commerce_price\Comparator\NumberComparator;
use Drupal\commerce_price\Comparator\PriceComparator;
use Drupal\commerce_store\StoreCreationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\commerce\Traits\DeprecationSuppressionTrait;
use Drupal\Tests\graphql_core_schema\Kernel\CoreComposableKernelTestBase;
use SebastianBergmann\Comparator\Factory as PhpUnitComparatorFactory;

/**
 * Base class for commerce order related graphql tests.
 *
 * Mostly taken from \Drupal\Tests\commerce\Kernel\CommerceKernelTestBase.
 */
abstract class GraphQLCommerceKernelTestBase extends CoreComposableKernelTestBase {

  use DeprecationSuppressionTrait;
  use StoreCreationTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'address',
    'datetime',
    'entity',
    'options',
    'inline_entity_form',
    'views',
    'commerce',
    'commerce_price',
    'commerce_store',
    'path',
    'path_alias',
    'graphql_commerce',
  ];

  /**
   * The default store.
   *
   * @var \Drupal\commerce_store\Entity\StoreInterface
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setErrorHandler();

    $factory = PhpUnitComparatorFactory::getInstance();
    $factory->register(new NumberComparator());
    $factory->register(new PriceComparator());

    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('commerce_currency');
    $this->installEntitySchema('commerce_store');
    $this->installConfig(['commerce_store']);

    $currency_importer = $this->container->get('commerce_price.currency_importer');
    $currency_importer->import('USD');

    $this->store = $this->createStore('Default store', 'admin@example.com');
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->restoreErrorHandler();
    parent::tearDown();
  }

}
