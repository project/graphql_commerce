<?php

declare(strict_types=1);

namespace Drupal\Tests\graphql_commerce\Kernel\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Tests\graphql\Traits\DataProducerExecutionTrait;
use Drupal\Tests\graphql_commerce\Kernel\GraphQLCommerceOrderKernelTestBase;

/**
 * Tests the add_to_cart data producer.
 *
 * @coversDefaultClass \Drupal\graphql_commerce\Plugin\GraphQL\DataProducer\AddToCart
 * @group graphql_commerce
 */
class AddToCartTest extends GraphQLCommerceOrderKernelTestBase {

  use DataProducerExecutionTrait;

  /**
   * Tests adding items to the cart.
   *
   * @covers ::resolve
   */
  public function testResolveAddToCart(): void {
    $added_quantity = 1;
    $order = $this->cartProvider->createCart('default', NULL, $this->user);
    $this->resolveAddToCartUrl($order, $this->variation->id(), $added_quantity);
    $actual_quantity = 0;
    $items = $order->getItems();
    if (!empty($items)) {
      $actual_quantity = $items[0]->getQuantity();
    }
    $this->assertEquals($added_quantity, $actual_quantity);

    // Add 2 more items of the same variation.
    $additional_quantity = 2;
    $this->resolveAddToCartUrl($order, $this->variation->id(), $additional_quantity);
    if (!empty($items)) {
      $actual_quantity = $items[0]->getQuantity();
    }
    $this->assertEquals($added_quantity + $additional_quantity, $actual_quantity);
  }

  /**
   * Execute the add to cart data producer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The cart order.
   * @param int|string $variation_id
   *   The variation id to add to the cart.
   * @param int|string $quantity
   *   The quantity to add.
   */
  private function resolveAddToCartUrl(OrderInterface $order, int|string $variation_id, int|string $quantity) {
    return $this->executeDataProducer('commerce_add_to_cart', [
      'order' => $order,
      'id' => $variation_id,
      'quantity' => $quantity,
    ]);
  }

}
