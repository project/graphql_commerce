<?php

namespace Drupal\graphql_commerce_promotion\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;

/**
 * Remove a coupon from the given order.
 *
 * @DataProducer(
 *   id = "commerce_promotion_remove_coupon",
 *   name = @Translation("Commerce Promotion: Remove Coupon"),
 *   description = @Translation("Removes a coupon on the given order."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation result.")
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *     "couponCode" = @ContextDefinition("string",
 *       label = @Translation("The coupon to remove."),
 *     )
 *   }
 * )
 */
class RemoveCoupon extends CommerceCartDataProducerBase {

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order.
   * @param string $couponCode
   *   The coupon to remove.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $field
   *   The field context.
   *
   * @return \Drupal\graphql_commerce\Wrappers\CommerceMutationResult
   *   The result.
   */
  public function resolve(OrderInterface $order, string $couponCode, FieldContext $field) {
    $context = new RenderContext();

    $result = $this->renderer->executeInRenderContext($context, function () use ($order, $couponCode) {
      $result = new CommerceMutationResult($order);

      /** @var \Drupal\commerce_promotion\CouponStorageInterface $couponStorage */
      $couponStorage = $this->entityTypeManager->getStorage('commerce_promotion_coupon');
      $coupon = $couponStorage->loadEnabledByCode($couponCode);

      if (empty($coupon)) {
        return $result->addError($this->t('The provided coupon code is invalid.'));
      }
      $id = $coupon->id();

      $couponIds = array_column($order->get('coupons')->getValue(), 'target_id');
      $index = array_search($id, $couponIds);

      if ($index === FALSE) {
        return $result->addError($this->t('The provided coupon has not been applied to this order.'));
      }

      $order->get('coupons')->removeItem($index);
      $order->save();

      return $result;
    });

    $field->addCacheableDependency($context);
    return $result;
  }

}
