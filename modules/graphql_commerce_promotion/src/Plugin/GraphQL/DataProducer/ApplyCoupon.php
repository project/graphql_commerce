<?php

namespace Drupal\graphql_commerce_promotion\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;

/**
 * Applies a coupon on the given order.
 *
 * @DataProducer(
 *   id = "commerce_promotion_apply_coupon",
 *   name = @Translation("Commerce Promotion: Apply Coupon"),
 *   description = @Translation("Applies a coupon on the given order."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation result.")
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *     "couponCode" = @ContextDefinition("string",
 *       label = @Translation("The coupon to apply."),
 *     )
 *   }
 * )
 */
class ApplyCoupon extends CommerceCartDataProducerBase {

  /**
   * Resolves the coupon code for an order and applies it if applicable.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity to apply the coupon code to.
   * @param string $couponCode
   *   The coupon code to apply.
   * @param FieldContext $field
   *   The field context object.
   *
   * @return \Drupal\commerce_order\CommerceMutationResult
   *   The result of applying the coupon code.
   */
  public function resolve(OrderInterface $order, string $couponCode, FieldContext $field) {
    $context = new RenderContext();

    $result = $this->renderer->executeInRenderContext($context, function () use ($order, $couponCode) {
      $result = new CommerceMutationResult($order);

      /** @var \Drupal\commerce_promotion\CouponStorageInterface $couponStorage */
      $couponStorage = $this->entityTypeManager->getStorage('commerce_promotion_coupon');
      $coupon = $couponStorage->loadEnabledByCode($couponCode);

      // Check if coupon exists.
      if (empty($coupon)) {
        return $result->addError($this->t('The provided coupon code is invalid.'));
      }

      // Check if the coupon has already been applied.
      foreach ($order->get('coupons') as $item) {
        if ($item->target_id == $coupon->id()) {
          return $result->addError($this->t('The provided coupon has already been applied.'));
        }
      }

      // Check if the coupon is available.
      if (!$coupon->available($order)) {
        return $result->addError($this->t('The provided coupon code is not available. It may have expired or already been used.'));
      }

      // Check if the coupon applies to this order.
      if (!$coupon->getPromotion()->applies($order)) {
        return $result->addError($this->t('The provided coupon code cannot be applied to your order.'));
      }

      // Add coupon and save order.
      $order->get('coupons')->appendItem($coupon->id());
      $order->save();

      return $result;
    });

    $field->addCacheableDependency($context);
    return $result;
  }

}
