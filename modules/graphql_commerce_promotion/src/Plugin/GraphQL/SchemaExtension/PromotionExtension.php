<?php

namespace Drupal\graphql_commerce_promotion\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\TypeAwareSchemaExtensionInterface;

/**
 * The commerce_promotion schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce_promotion",
 *   name = "Commerce Promotion",
 *   description = "Integration for commerce_promotion.",
 *   schema = "core_composable"
 * )
 */
class PromotionExtension extends SdlSchemaExtensionPluginBase implements TypeAwareSchemaExtensionInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['commerce_promotion', 'commerce_promotion_coupon'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeExtensionDefinition(array $types) {
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Mutation', 'commerceApplyCoupon',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE)),
        $builder->produce('commerce_promotion_apply_coupon')
          ->map('order', $builder->fromParent())
          ->map('couponCode', $builder->fromArgument('couponCode')),
      )
    );

    $registry->addFieldResolver('Mutation', 'commerceRemoveCoupon',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE)),
        $builder->produce('commerce_promotion_remove_coupon')
          ->map('order', $builder->fromParent())
          ->map('couponCode', $builder->fromArgument('couponCode')),
      )
    );

  }

}
