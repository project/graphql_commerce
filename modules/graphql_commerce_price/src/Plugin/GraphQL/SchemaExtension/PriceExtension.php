<?php

namespace Drupal\graphql_commerce_price\Plugin\GraphQL\SchemaExtension;

use Drupal\commerce_order\AdjustmentTypeManager;
use Drupal\commerce_price\Price;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\TypeAwareSchemaExtensionInterface;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Schema;
use GraphQL\Utils\SchemaPrinter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The commerce price schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce_price",
 *   name = "Commerce Price",
 *   description = "Functionality for price handling.",
 *   schema = "core_composable"
 * )
 */
class PriceExtension extends SdlSchemaExtensionPluginBase implements TypeAwareSchemaExtensionInterface, ContainerFactoryPluginInterface {

  /**
   * The adjustment type manager.2.
   *
   * @var \Drupal\commerce_order\AdjustmentTypeManager
   */
  protected AdjustmentTypeManager $adjustmentTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('plugin.manager.commerce_adjustment_type'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\commerce_order\AdjustmentTypeManager $adjustmentTypeManager
   *   The adjustment type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    AdjustmentTypeManager $adjustmentTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);
    $this->adjustmentTypeManager = $adjustmentTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['commerce_checkout'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return ['commerce'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseDefinition() {
    // Build the enum for the available adjustment types.
    $definitions = $this->adjustmentTypeManager->getDefinitions();
    $values = [];

    foreach ($definitions as $id => $definition) {
      $key = strtoupper(
          str_replace(
              '.',
              '_',
              str_replace('-', '_', $id)
          ));

      $values[$key] = [
        'value' => $key,
        'description' => (string) $definition['label'],
      ];
    }
    $schema = new Schema([
      'types' => [
        new EnumType([
          'name' => 'CommerceAdjustmentType',
          'values' => $values,
          'description' => 'Available adjustment types.',
        ]),
      ],
    ]);

    return SchemaPrinter::doPrint($schema) . "\n\n" . parent::getBaseDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeExtensionDefinition(array $types) {
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'CommercePrice',
      'number',
      $builder->callback(function ($price) {
        if (is_array($price)) {
          $price = Price::fromArray($price);
        }
        return $price->getNumber();
      })
    );

    $registry->addFieldResolver(
      'CommercePrice',
      'currencyCode',
      $builder->callback(function ($price) {
        if (is_array($price)) {
          $price = Price::fromArray($price);
        }
        return $price->getCurrencyCode();
      })
    );

    $registry->addFieldResolver(
      'CommercePrice',
      'formatted',
      $builder->produce('commerce_formatted_price')
        ->map('price', $builder->fromParent())
    );

    $registry->addFieldResolver('CommerceProductVariation', 'computedPrice',
      $builder->produce('commerce_calculated_price')
        ->map('item', $builder->fromParent())
        ->map('adjustmentTypes', $builder->fromArgument('adjustments'))
    );
  }

}
