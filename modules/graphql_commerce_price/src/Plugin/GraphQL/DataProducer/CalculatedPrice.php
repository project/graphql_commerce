<?php

namespace Drupal\graphql_commerce_price\Plugin\GraphQL\DataProducer;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\AdjustmentTypeManager;
use Drupal\commerce_order\PriceCalculatorInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces a calculated price.
 *
 * @DataProducer(
 *   id = "commerce_calculated_price",
 *   name = @Translation("Commerce: Calculated Price"),
 *   description = @Translation("Produces the calculated price."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The price object.")
 *   ),
 *   consumes = {
 *     "item" = @ContextDefinition("any",
 *       label = @Translation("The PriceItem field item."),
 *     ),
 *     "adjustmentTypes" = @ContextDefinition("any",
 *       label = @Translation("The adjustment types to use."),
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class CalculatedPrice extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The price calculator.
   *
   * @var \Drupal\commerce_order\PriceCalculatorInterface
   */
  protected PriceCalculatorInterface $priceCalculator;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected CurrentStoreInterface $currentStore;

  /**
   * The available adjustment types.
   *
   * This is used to sort the given adjustment types from the GraphQL query, as
   * they might not be in the desired order.
   *
   * @var string[]
   */
  protected array $adjustmentTypes;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_order.price_calculator'),
      $container->get('commerce_store.current_store'),
      $container->get('current_user'),
      $container->get('plugin.manager.commerce_adjustment_type'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_order\PriceCalculatorInterface $priceCalculator
   *   The price calculator.
   * @param \Drupal\commerce_store\CurrentStoreInterface $currentStore
   *   The current store.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\commerce_order\AdjustmentTypeManager $adjustmentTypeManager
   *   The adjustment type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    PriceCalculatorInterface $priceCalculator,
    CurrentStoreInterface $currentStore,
    AccountInterface $currentUser,
    AdjustmentTypeManager $adjustmentTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->priceCalculator = $priceCalculator;
    $this->currentStore = $currentStore;
    $this->currentUser = $currentUser;
    $this->adjustmentTypes = array_keys($adjustmentTypeManager->getDefinitions());
  }

  /**
   * Resolves the price of a purchasable entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $purchasableEntity
   *   The purchasable entity for which to resolve the price.
   * @param array|null $adjustmentTypes
   *   (optional) An array of adjustment types to apply to the price.
   * @param FieldContext $field
   *   The field context object.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The resolved price of the purchasable entity, or NULL if not available.
   */
  public function resolve(PurchasableEntityInterface $purchasableEntity, array $adjustmentTypes = NULL, FieldContext $field) {
    $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, []);

    $adjustmentTypes = $this->mapAdjustmentTypes($adjustmentTypes ?? []);

    $result = $this->priceCalculator->calculate($purchasableEntity, 1, $context, $adjustmentTypes);
    $price = $result->getCalculatedPrice();

    $field->addCacheableDependency($purchasableEntity);
    $field->addCacheContexts([
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'country',
    ]);
    return $price;
  }

  /**
   * Get the sorted adjustment types.
   *
   * The CalculatedPrice field formatter does not sort the adjustment types
   * first and only relies on the order that they are defined in the
   * configuration. This order is implied by the order given from the
   * adjustment_type plugin manager getDefinitions() result.
   * This method basically does the same at runtime and filters and sorts the
   * given adjustment types first.
   *
   * @param string[] $adjustmentTypes
   *   Array of enum adjustment types in uppercase.
   *
   * @return string[]
   *   The mapped, filtered and sorted adjustment types.
   */
  private function mapAdjustmentTypes(array $adjustmentTypes = []): array {
    if (empty($adjustmentTypes)) {
      return [];
    }

    // Convert enum values (e.g. SHIPPING_PROMOTION => shipping_promotion).
    $types = array_map('strtolower', $adjustmentTypes);

    // Generate filtered and sorted array.
    $result = [];
    foreach ($this->adjustmentTypes as $id) {
      if (in_array($id, $types, TRUE)) {
        $result[] = $id;
      }
    }

    return $result;
  }

}
