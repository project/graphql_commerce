<?php

namespace Drupal\graphql_commerce_price\Plugin\GraphQL\DataProducer;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces a formatted price.
 *
 * @DataProducer(
 *   id = "commerce_formatted_price",
 *   name = @Translation("Commerce: Formatted Price"),
 *   description = @Translation("Produces the formatted price."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("The formatted price.")
 *   ),
 *   consumes = {
 *     "price" = @ContextDefinition("any",
 *       label = @Translation("The Price object."),
 *     ),
 *   }
 * )
 */
class FormattedPrice extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected CurrencyFormatterInterface $currencyFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_price.currency_formatter')
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_price\CurrencyFormatter $currencyFormatter
   *   The currency formatter.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    CurrencyFormatterInterface $currencyFormatter
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->currencyFormatter = $currencyFormatter;
  }

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_price\Price|array $price
   *   The price.
   *
   * @return string
   *   The formatted price.
   */
  public function resolve($price) {
    if (is_array($price)) {
      $price = Price::fromArray($price);
    }
    return $this->currencyFormatter->format(
      $price->getNumber(),
      $price->getCurrencyCode(),
    );
  }

}
