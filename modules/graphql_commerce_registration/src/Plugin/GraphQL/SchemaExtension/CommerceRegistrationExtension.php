<?php

namespace Drupal\graphql_commerce_registration\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\TypeAwareSchemaExtensionInterface;

/**
 * The commerce_promotion schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce_registration",
 *   name = "Commerce Registration",
 *   description = "Integration for commerce_registration.",
 *   schema = "core_composable"
 * )
 */
class CommerceRegistrationExtension extends SdlSchemaExtensionPluginBase implements TypeAwareSchemaExtensionInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['registration_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return ['commerce_registration'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeExtensionDefinition(array $types) {
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();
    $registry->addFieldResolver(
      'CommerceProductVariationEvent',
      'registrationSettings',
      $builder->compose(
        $builder->produce('registration_settings')
          ->map('variation', $builder->fromParent()),
      )
    );
  }

}
