<?php

namespace Drupal\graphql_commerce_registration\Plugin\GraphQL\DataProducer;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get the registration settings of a given host entity.
 *
 * @DataProducer(
 *   id = "registration_settings",
 *   name = @Translation("Registration Settings of host entity"),
 *   description = @Translation("Get the registration settings of a host entity."),
 *   produces = @ContextDefinition("entity",
 *     label = @Translation("The registration setting.")
 *   ),
 *   consumes = {
 *     "variation" = @ContextDefinition("entity:commerce_product_variation",
 *       label = @Translation("The commerce product variation."),
 *     ),
 *   }
 * )
 */
class RegistrationSettings extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   The commerce product variation.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $field
   *   The field context.
   *
   * @return mixed
   *   The registration settings.
   */
  public function resolve(ProductVariationInterface $variation, FieldContext $field) {
    $context = new RenderContext();

    $result = $this->renderer->executeInRenderContext($context, function () use ($variation) {
      $handler = $this->entityTypeManager->getHandler('registration', 'host_entity');
      $host_entity = $handler->createHostEntity($variation);
      if ($host_entity->isConfiguredForRegistration()) {
        $settings = $host_entity->getSettings();
        return $settings;
      }
      return NULL;
    });

    $field->addCacheableDependency($context);
    return $result;
  }

}
