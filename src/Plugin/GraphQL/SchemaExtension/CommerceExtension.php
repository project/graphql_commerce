<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\SchemaExtension;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Plugin\Field\FieldType\AdjustmentItem;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;
use Drupal\graphql_core_schema\TypeAwareSchemaExtensionInterface;

/**
 * The commerce schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce",
 *   name = "Commerce",
 *   description = "Improved commerce integration.",
 *   schema = "core_composable"
 * )
 */
class CommerceExtension extends SdlSchemaExtensionPluginBase implements TypeAwareSchemaExtensionInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['commerce_product', 'commerce_product_variation'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeExtensionDefinition(array $types) {
    if (in_array('FieldItemTypeCommerceAdjustment', $types)) {
      return $this->loadDefinitionFile('FieldItemTypeCommerceAdjustment');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'Query',
      'commerceGetCart',
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE))
    );

    $registry->addFieldResolver(
      'Mutation',
      'commerceAddToCart',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(TRUE)),
        $builder->produce('commerce_add_to_cart')
          ->map('order', $builder->fromParent())
          ->map('id', $builder->fromArgument('productVariationId'))
          ->map('quantity', $builder->fromArgument('quantity'))
      )
    );

    $registry->addFieldResolver(
      'Mutation',
      'commerceRemoveFromCart',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE)),
        $builder->produce('commerce_remove_from_cart')
          ->map('order', $builder->fromParent())
          ->map('id', $builder->fromArgument('orderItemId'))
      )
    );

    $registry->addFieldResolver(
      'Mutation',
      'commerceCartChangeQuantity',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE)),
        $builder->produce('commerce_cart_change_quantity')
          ->map('order', $builder->fromParent())
          ->map('id', $builder->fromArgument('orderItemId'))
          ->map('quantity', $builder->fromArgument('quantity'))
      )
    );

    $registry->addFieldResolver(
      'Mutation',
      'commerceEmptyCart',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE)),
        $builder->produce('commerce_empty_cart')
          ->map('order', $builder->fromParent())
      )
    );

    // Field resolvers for the CommerceMutationResult.
    $registry->addFieldResolver(
      'CommerceMutationResult',
      'success',
      $builder->callback(function ($value) {
        return $value->isSuccess();
      })
    );

    $registry->addFieldResolver(
      'CommerceMutationResult',
      'errors',
      $builder->callback(function (CommerceMutationResult $value) {
        return $value->getErrors();
      })
    );

    $registry->addFieldResolver(
      'CommerceProduct',
      'variation',
      $builder->callback(function (ProductInterface $product, $args) {
        $id = $args['id'] ?? NULL;

        if ($id) {
          $id = (string) $id;
          $variations = $product->getVariations();
          foreach ($variations as $variation) {
            $variationId = (string) $variation->id();
            if ($variationId === $id) {
              return $variation;
            }
          }
        }

        return $product->getDefaultVariation();
      })
    );

    $registry->addFieldResolver(
      'CommerceMutationResult',
      'order',
      $builder->callback(function (CommerceMutationResult $value) {
        return $value->getOrder();
      })
    );

    $registry->addFieldResolver(
      'CommerceOrder',
      'orderTotalSummary',
      $builder->produce('commerce_order_total_summary')
        ->map('order', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'CommerceOrderTotalSummary',
      'subtotal',
      $builder->callback(function (array $value) {
        return $value['subtotal'];
      })
    );

    $registry->addFieldResolver(
      'CommerceOrderTotalSummary',
      'total',
      $builder->callback(function (array $value) {
        return $value['total'];
      })
    );

    $registry->addFieldResolver(
      'CommerceOrderTotalSummary',
      'totalToString',
      $builder->callback(function (array $value) {
        return (string) $value['total'];
      })
    );

    $registry->addFieldResolver(
      'CommerceOrderTotalSummary',
      'subTotalToString',
      $builder->callback(function (array $value) {
        return (string) $value['subtotal'];
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'type',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->getType();
        }
        return $adjustment['type'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'label',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->getLabel();
        }
        return $adjustment['label'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'amount',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->getAmount();
        }
        return $adjustment['amount'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'percentage',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->getPercentage();
        }
        return $adjustment['percentage'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'sourceId',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->getSourceId();
        }
        return $adjustment['source_id'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'included',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->isIncluded();
        }
        return $adjustment['included'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'CommerceAdjustment',
      'locked',
      $builder->callback(function ($adjustment) {
        if ($adjustment instanceof Adjustment) {
          return $adjustment->isLocked();
        }
        return $adjustment['locked'] ?? NULL;
      })
    );

    $registry->addFieldResolver(
      'FieldItemTypeCommerceAdjustment',
      'adjustment',
      $builder->callback(function (AdjustmentItem $item) {
        return $item->value;
      })
    );
  }

}
