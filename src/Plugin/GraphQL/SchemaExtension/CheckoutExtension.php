<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\SchemaExtension;

use Drupal\commerce_checkout\CheckoutPaneManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\EntitySchemaHelper;
use Drupal\graphql_core_schema\TypeAwareSchemaExtensionInterface;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use GraphQL\Utils\SchemaPrinter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The commerce checkout schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce_checkout",
 *   name = "Commerce Checkout",
 *   description = "Integration for commerce_checkout.",
 *   schema = "core_composable"
 * )
 */
class CheckoutExtension extends SdlSchemaExtensionPluginBase implements TypeAwareSchemaExtensionInterface {

  /**
   * The adjustment type manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutPaneManager
   */
  protected CheckoutPaneManager $checkoutPaneManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('plugin.manager.commerce_checkout_pane'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\commerce_checkout\CheckoutPaneManager $checkoutPaneManager
   *   The adjustment type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    CheckoutPaneManager $checkoutPaneManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);
    $this->checkoutPaneManager = $checkoutPaneManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['commerce_checkout'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeExtensionDefinition(array $types) {
    $extensions = [
      $this->loadDefinitionFile('extension'),
    ];
    if ($this->checkoutPaneManager->hasDefinition('shipping_information')) {
      $extensions[] = $this->loadDefinitionFile('CommerceCheckoutPaneShippingInformation');
    }
    return implode("\n", array_filter($extensions));
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseDefinition() {
    $definitions = $this->checkoutPaneManager->getDefinitions();
    $interfaceFields = [
      'id' => Type::string(),
      'label' => Type::string(),
      'displayLabel' => Type::string(),
      'visible' => Type::boolean(),
    ];
    $types = [];
    $interface = new InterfaceType([
      'name' => 'CommerceCheckoutPane',
      'fields' => $interfaceFields,
    ]);

    foreach ($definitions as $id => $definition) {
      $typeName = EntitySchemaHelper::toPascalCase([
        'commerce_checkout_pane',
        $id,
      ]);
      $types[$typeName] = new ObjectType([
        'name' => $typeName,
        'description' => (string) $definition['label'],
        'fields' => $interfaceFields,
        'interfaces' => [$interface],
      ]);
    }
    $schema = new Schema([
      'types' => $types,
    ]);

    $base = $this->loadDefinitionFile('base');
    return $base . "\n" . SchemaPrinter::doPrint($schema);
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Query', 'commerceCheckout',
      $builder->compose(
        $builder->produce('commerce_get_cart')
          ->map('create', $builder->fromValue(FALSE)),
        $builder->produce('commerce_checkout')
          ->map('order', $builder->fromParent())
          ->map('step', $builder->fromArgument('step')),
      )
    );

    $registry->addFieldResolver('CommerceCheckout', 'order',
      $builder->callback(function (CheckoutFlowInterface $flow) {
        return $flow->getOrder();
      })
    );

    $registry->addFieldResolver('Mutation', 'commerceAddProfileForm',
      $builder->compose(
        $builder->produce('form_entity_create')
          ->map('entityType', $builder->fromValue('profile'))
          ->map('values', $builder->produce('commerce_profile_create_data')),
        $builder->produce('form_result')
          ->map('form', $builder->fromParent())
          ->map('formState', $builder->fromContext('form_state'))
          ->map('input', $builder->fromArgument('input'))
          ->map('excludeTypes', $builder->fromValue([
            'address',
            'address_country',
          ])),
      )
    );

    $registry->addFieldResolver('Mutation', 'commerceEditProfileForm',
      $builder->compose(
        $builder->produce('entity_load')
          ->map('type', $builder->fromValue('profile'))
          ->map('id', $builder->fromArgument('id'))
          ->map('access_operation', $builder->fromArgument('update')),
        $builder->produce('form_entity_edit')
          ->map('entity', $builder->fromParent())
          ->map('operation', $builder->fromValue('address-book-edit')),
        $builder->produce('form_result')
          ->map('form', $builder->fromParent())
          ->map('formState', $builder->fromContext('form_state'))
          ->map('input', $builder->fromArgument('input'))
          ->map('excludeTypes', $builder->fromValue([
            'address',
            'address_country',
          ])),
      )
    );

    $registry->addFieldResolver('Mutation', 'commerceShippingForm',
      $builder->compose(
        $builder->produce('commerce_shipping_form')
          ->map('checkout',
            $builder->compose(
              $builder->produce('commerce_get_cart')
                ->map('create', $builder->fromValue(FALSE)),
              $builder->produce('commerce_checkout')
                ->map('order', $builder->fromParent())
            )
          ),
        $builder->produce('form_result')
          ->map('form', $builder->fromParent())
          ->map('formState', $builder->fromContext('form_state'))
          ->map('input', $builder->fromArgument('input'))
          ->map('excludeTypes', $builder->fromValue([
            'address',
            'address_country',
          ])),
      )
    );

    $this->registerStepResolvers($registry, $builder);
    $this->registerPaneResolvers($registry, $builder);
  }

  /**
   * Register the checkout pane resolvers.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The resolver builder.
   */
  private function registerPaneResolvers(ResolverRegistryInterface $registry, ResolverBuilder $builder) {
    $registry->addTypeResolver('CommerceCheckoutPane', function (CheckoutPaneInterface $pane) {
      $id = $pane->getId();
      return EntitySchemaHelper::toPascalCase(['CommerceCheckoutPane', $id]);
    });

    $registry->addFieldResolver('CommerceCheckout', 'panes',
      $builder->callback(function (CheckoutFlowInterface $flow, $args, ResolveContext $context, ResolveInfo $info) {
        if ($flow instanceof CheckoutFlowWithPanesInterface) {
          $currentStep = $context->getContextValue($info, 'commerce_checkout_step');
          if (!empty($args['onlyVisible']) && $currentStep) {
            return $flow->getVisiblePanes($currentStep);
          }
          return $flow->getPanes();
        }
        return [];
      })
    );

    $registry->addFieldResolver('CommerceCheckoutPane', 'id',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return $pane->getId();
      })
    );
    $registry->addFieldResolver('CommerceCheckoutPane', 'label',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return $pane->getLabel();
      })
    );
    $registry->addFieldResolver('CommerceCheckoutPane', 'displayLabel',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return $pane->getDisplayLabel();
      })
    );
    $registry->addFieldResolver('CommerceCheckoutPane', 'visible',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return $pane->isVisible();
      })
    );

    $registry->addFieldResolver('CommerceCheckoutPaneShippingInformation', 'requireShippingProfile',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return !empty($pane->getConfiguration()['require_shipping_profile']);
      })
    );

    $registry->addFieldResolver('CommerceCheckoutPaneShippingInformation', 'autoRecalculate',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return !empty($pane->getConfiguration()['auto_recalculate']);
      })
    );

    $registry->addFieldResolver('CommerceCheckoutPaneShippingInformation', 'form',
      $builder->callback(function (CheckoutPaneInterface $pane) {
        return NULL;
        // Return $pane->buildPaneForm([],);.
      })
    );
  }

  /**
   * Register the checkout step resolvers.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   The resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The resolver builder.
   */
  private function registerStepResolvers(ResolverRegistryInterface $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('CommerceCheckout', 'steps',
      $builder->callback(function (CheckoutFlowInterface $flow) {
        $steps = $flow->getSteps();
        foreach (array_keys($steps) as $id) {
          $steps[$id]['id'] = $id;
        }
        return $steps;
      })
    );
    $registry->addFieldResolver('CommerceCheckout', 'currentStep',
      $builder->callback(function (CheckoutFlowInterface $flow, $args, ResolveContext $context, ResolveInfo $info) {
        $id = $context->getContextValue($info, 'commerce_checkout_step');
        $steps = $flow->getSteps();
        $step = $steps[$id] ?? NULL;
        if ($step) {
          $step['id'] = $id;
        }
        return $step;
      })
    );

    $registry->addFieldResolver('CommerceCheckout', 'nextStep',
      $builder->callback(function (CheckoutFlowInterface $flow, $args, ResolveContext $context, ResolveInfo $info) {
        $currentStep = $context->getContextValue($info, 'commerce_checkout_step');
        $nextStep = $flow->getNextStepId($currentStep);
        $steps = $flow->getSteps();
        $step = NULL;
        if ($nextStep) {
          $step = $steps[$nextStep] ?? NULL;
          if ($step) {
            $step['id'] = $nextStep;
          }
        }
        return $step;
      })
    );

    $registry->addFieldResolver('CommerceCheckout', 'previousStep',
      $builder->callback(function (CheckoutFlowInterface $flow, $args, ResolveContext $context, ResolveInfo $info) {
        $currentStep = $context->getContextValue($info, 'commerce_checkout_step');
        $previousStep = $flow->getPreviousStepId($currentStep);
        $steps = $flow->getSteps();
        $step = NULL;
        if ($previousStep) {
          $step = $steps[$previousStep] ?? NULL;
          if ($step) {
            $step['id'] = $previousStep;
          }
        }
        return $step;
      })
    );
  }

}
