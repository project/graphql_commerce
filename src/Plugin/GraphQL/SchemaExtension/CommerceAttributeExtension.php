<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * The commerce attribute schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce_attribute",
 *   name = "Commerce Attribute",
 *   description = "Improved support for product attributes.",
 *   schema = "core_composable"
 * )
 */
class CommerceAttributeExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return [
      'commerce_product_variation',
      'commerce_product_attribute',
      'commerce_product_attribute_value',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return ['commerce'];
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('CommerceProductVariation', 'attributes',
      $builder->produce('commerce_product_attributes')->map('variation', $builder->fromParent())
    );

    $registry->addFieldResolver('CommercePreparedAttribute', 'elementType',
      $builder->callback(function (array $attribute) {
        return $attribute['element_type'];
      })
    );

    $registry->addFieldResolver('CommercePreparedAttribute', 'values',
      $builder
        ->produce('commerce_prepared_attribute_values')
        ->map('preparedAttribute', $builder->fromParent())
    );

    $registry->addFieldResolver('CommercePreparedAttributeValue', 'id',
      $builder->callback(function (array $values) {
        return $values['id'] ?? NULL;
      })
    );

    $registry->addFieldResolver('CommercePreparedAttributeValue', 'label',
      $builder->callback(function (array $values) {
        return $values['label'] ?? NULL;
      })
    );

    $registry->addFieldResolver('CommercePreparedAttributeValue', 'variation',
      $builder->callback(function (array $values, $args, ResolveContext $context, ResolveInfo $info, FieldContext $field) {
        // @todo This looks very hacky. There's probably a better way to get a
        // matching variation here.
        // Get the current product variation from context.
        /** @var \Drupal\commerce_product\Entity\ProductVariation|null $variation */
        $variation = $context->getContextValue($info, 'commerce_product_variation');

        if (!$variation) {
          return NULL;
        }

        /** @var \Drupal\commerce_product\Entity\ProductVariation[] $variations */
        $attributes = $variation->getAttributeValueIds();

        /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
        $storage = \Drupal::service('entity_type.manager')->getStorage('commerce_product_variation');
        $searchAttributes = [
          $values['attribute']['field_name'] => $values['id'],
          'product_id' => $variation->getProductId(),
        ];

        // Properties to search for.
        $properties = array_merge($attributes, $searchAttributes);
        $entities = array_values($storage->loadByProperties($properties));
        $matchingVariation = $entities[0] ?? NULL;

        if ($matchingVariation) {
          return $matchingVariation;
        }

        // Find a variation that has the new property only.
        $entities = array_values($storage->loadByProperties($searchAttributes));
        return $entities[0] ?? NULL;
      })
    );
  }

}
