<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\SchemaExtension;

use Drupal\commerce_order\AvailabilityResult;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\EntitySchemaBuilder;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

/**
 * The commerce schema extension.
 *
 * @SchemaExtension(
 *   id = "commerce_availability",
 *   name = "Commerce Availability",
 *   description = "Provides integration of computed availability.",
 *   schema = "core_composable"
 * )
 */
class CommerceAvailabilityExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['commerce_product_variation'];
  }

  /**
   * {@inheritdoc}
   */
  public function getInterfaceExtender() {
    return [
      'CommerceProductVariation' => [
        function (EntitySchemaBuilder $builder, array &$fields) {
          $fields['computedAvailability'] = [
            'type' => new ObjectType(
              [
                'name' => 'CommerceAvailabilityResult',
                'fields' => [
                  'reason' => Type::string(),
                  'code' => Type::string(),
                  'isNeutral' => Type::boolean(),
                  'isUnavailable' => Type::boolean(),
                ],
              ]
            ),
            'args' => [
              'quantity' => [
                'type' => Type::int(),
                'description' => 'The quantity of product variations.',
              ],
            ],
            'description' => 'Load the variation of the given ID. Falls back to the default variation.',
          ];
        },
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'CommerceProductVariation',
      'computedAvailability',
      $builder->compose(
        $builder->produce('commerce_computed_availability')
          ->map('productVariation', $builder->fromParent())
          ->map('quantity', $builder->fromArgument('quantity'))
      )
    );

    $registry->addFieldResolver(
      'CommerceAvailabilityResult',
      'reason',
      $builder->callback(function (AvailabilityResult $value) {
        return $value->getReason();
      })
    );

    $registry->addFieldResolver(
      'CommerceAvailabilityResult',
      'code',
      $builder->callback(function (AvailabilityResult $value) {
        return $value->getCode();
      })
    );

    $registry->addFieldResolver(
      'CommerceAvailabilityResult',
      'isNeutral',
      $builder->callback(function (AvailabilityResult $value) {
        return $value->isNeutral();
      })
    );

    $registry->addFieldResolver(
      'CommerceAvailabilityResult',
      'isUnavailable',
      $builder->callback(function (AvailabilityResult $value) {
        return $value->isUnavailable();
      })
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getBaseDefinition() {
    return '';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getExtensionDefinition() {
    return '';
  }

}
