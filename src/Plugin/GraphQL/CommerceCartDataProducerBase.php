<?php

declare(strict_types=1);

namespace Drupal\graphql_commerce\Plugin\GraphQL;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\AvailabilityManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for commerce cart data producers.
 */
class CommerceCartDataProducerBase extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_cart.cart_manager'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('commerce_order.availability_manager'),
      $container->get('commerce_store.current_store'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cartManager
   *   The cart manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cartProvider
   *   The cart provider.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\commerce_order\AvailabilityManagerInterface $availabilityManager
   *   The availability manager.
   * @param \Drupal\commerce_store\CurrentStoreInterface $currentStore
   *   The current store.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    protected CartManagerInterface $cartManager,
    protected CartProviderInterface $cartProvider,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RendererInterface $renderer,
    protected AvailabilityManagerInterface $availabilityManager,
    protected CurrentStoreInterface $currentStore,
    protected AccountProxyInterface $currentUser,
    protected MessengerInterface $messenger,
    protected ConfigFactoryInterface $configFactory
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Get availability context.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $entity
   *   Entity.
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   Order.
   *
   * @return \Drupal\commerce\Context
   *   Context.
   */
  protected function getAvailabilityContext(ProductVariationInterface $entity, OrderInterface $order = NULL): Context {
    // If we have an order we use that to determine the store. If not, we use
    // the current store. In either case we check if the variation is
    // offered in that store. If not, we default to the first store that the
    // variation is offered in. This is based on how the context is derived in
    // commerce_stock_enforcement, but is going to produce unexpected results
    // in multi store environments.
    // @todo Revisit multi store support.
    if ($order) {
      $storeToUse = $order->getStore();
    }
    else {
      $storeToUse = $this->currentStore->getStore();
    }
    // Make sure the current store is in the entity stores.
    $stores = $entity->getStores();
    $found = FALSE;
    // If we have a current store.
    if ($storeToUse) {
      // Make sure it is associated with the current product.
      foreach ($stores as $store) {
        if ((int) $store->id() === (int) $storeToUse->id()) {
          $found = TRUE;
          break;
        }
      }
    }
    // If not found and we have stores associated with the product.
    if (!$found && !empty($stores)) {
      // Get the first store the product is assigned to.
      $storeToUse = array_shift($stores);
    }
    return new Context($this->currentUser, $storeToUse);
  }

  /**
   * Add any stock violations.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $purchaseableEntity
   *   Purchaseable entity.
   * @param int $quantityDelta
   *   The change in quantity to validate.
   * @param \Drupal\graphql_commerce\Wrapper\CommerceMutationResult $result
   *   Result.
   *
   * @return bool
   *   Whether any violations were added.
   */
  protected function addStockViolationsToResult(
    PurchasableEntityInterface $purchaseableEntity,
    int $quantityDelta,
    CommerceMutationResult $result
  ): bool {

    if (!function_exists('commerce_stock_enforcement_get_stock_level')) {
      return FALSE;
    }
    $context = commerce_stock_enforcement_get_context($purchaseableEntity);

    // Get the available stock level.
    $stockLevel = commerce_stock_enforcement_get_stock_level($purchaseableEntity, $context);
    // Get the already ordered quantity.
    $alreadyOrdered = commerce_stock_enforcement_get_ordered_quantity($purchaseableEntity, $context);
    $totalRequested = $alreadyOrdered + $quantityDelta;
    if ($totalRequested <= $stockLevel) {
      return FALSE;
    }

    if ($alreadyOrdered === 0) {
      $messageText = $this->configFactory->get('commerce_stock_enforcement.settings')
        ->get('insufficient_stock_add_to_cart_zero_in_cart');
      $messageText = Xss::filter($messageText);
      $message = new FormattableMarkup($messageText, [
        '%qty' => $stockLevel,
        '%qty_asked' => $quantityDelta,
      ]);
    }
    else {
      $messageText = $this->configFactory->get('commerce_stock_enforcement.settings')
        ->get('insufficient_stock_add_to_cart_quantity_in_cart');
      $messageText = Xss::filter($messageText);
      $message = new FormattableMarkup($messageText, [
        '%qty' => $stockLevel,
        '%qty_o' => $alreadyOrdered,
      ]);
    }
    $result->addError((string) $message);
    return TRUE;
  }

  /**
   * Get the quantity delta.
   *
   * @param int $newQuantity
   *   New quantity in cart.
   * @param \Drupal\commerce\PurchasableEntityInterface $purchaseableEntity
   *   Purchaseable entity.
   *
   * @return int
   *   The quantity delta (+ve increase, -ve decrease).
   */
  protected function getQuantityDelta(int $newQuantity, PurchasableEntityInterface $purchaseableEntity): int {
    if (function_exists('commerce_stock_enforcement_get_stock_level')) {
      $context = commerce_stock_enforcement_get_context($purchaseableEntity);
      $existingQuantity = commerce_stock_enforcement_get_stock_level($purchaseableEntity, $context);
      // Commerce does not respect its return types.
      return (int) ($newQuantity - (int) $existingQuantity);
    }
    return 0;
  }

}
