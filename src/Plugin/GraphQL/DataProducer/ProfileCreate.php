<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\graphql_form_schema\Plugin\GraphQL\EntityFormBase;

/**
 * Builds the default data for creating a profile.
 *
 * @DataProducer(
 *   id = "commerce_profile_create_data",
 *   name = @Translation("Commerce: Profile Create"),
 *   description = @Translation("Builds a form to create an entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The initial data to create a profile.")
 *   ),
 * )
 */
class ProfileCreate extends EntityFormBase {

  /**
   * The resolver.
   *
   * @return array
   *   The default values when creating a new profile.
   */
  public function resolve() {
    $user = \Drupal::service('current_user');
    return [
      'uid' => $user->id(),
      'type' => 'customer',
    ];
  }

}
