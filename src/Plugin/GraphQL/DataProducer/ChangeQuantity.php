<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;

/**
 * The change quantity producer.
 *
 * @DataProducer(
 *   id = "commerce_cart_change_quantity",
 *   name = @Translation("Commerce Cart: Change Quantity"),
 *   description = @Translation("Change the quantity of an order item."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation result.")
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *     "id" = @ContextDefinition("any",
 *       label = @Translation("The product variation ID to change the quantity of."),
 *     ),
 *     "quantity" = @ContextDefinition("integer",
 *       label = @Translation("The new quantity."),
 *     )
 *   }
 * )
 */
class ChangeQuantity extends CommerceCartDataProducerBase {

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order.
   * @param string|int $id
   *   The product variation ID.
   * @param int $quantity
   *   The new quantity.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $field
   *   The field context.
   */
  public function resolve(OrderInterface $order, $id, int $quantity, FieldContext $field) {

    $result = new CommerceMutationResult($order);

    $storage = $this->entityTypeManager->getStorage('commerce_order_item');
    /** @var \Drupal\commerce_order\Entity\OrderItem|null $entity */
    $entity = $storage->load($id);
    if (!$entity) {
      return $result->addProductVariationNotFoundError();
    }

    $context = new RenderContext();

    $this->renderer->executeInRenderContext($context, function () use ($order, $entity, $quantity, $result) {
      $orderItem = $entity;
      if ($quantity === 0) {
        $this->cartManager->removeOrderItem($order, $orderItem, save_cart: TRUE);
        return;
      }
      $orderItem->setQuantity($quantity);
      // These could be related to availability or other sources.
      $violations = $orderItem->validate();
      if ($violations->count() === 0) {
        $quantityDelta = $this->getQuantityDelta($quantity, $orderItem->getPurchasedEntity());
        if (!$this->addStockViolationsToResult($orderItem->getPurchasedEntity(), $quantityDelta, $result)) {
          $this->cartManager->updateOrderItem($order, $orderItem, save_cart: TRUE);
        }
      }
      else {
        foreach ($violations as $violation) {
          $result->addError($violation->getMessage());
        }
      }
    });
    $field->addCacheableDependency($context);
    return $result;
  }

}
