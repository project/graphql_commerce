<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer\Checkout;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesInterface;
use Drupal\Core\Form\FormState;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_form_schema\Plugin\GraphQL\EntityFormBase;

/**
 * Class ShippingForm.
 *
 * Represents a shipping form in a checkout process.
 *
 * @package YourPackage
 */
class ShippingForm extends EntityFormBase {

  public function resolve(CheckoutFlowInterface $checkout, FieldContext $fieldContext) {
    if ($checkout instanceof CheckoutFlowWithPanesInterface) {
      $formState = new FormState();
      $formState->setProgrammed();
      $formState->setUserInput([]);
      $step_id = 'order_information';
      $formState->addBuildInfo('args', [$step_id]);
      $fieldContext->setContextValue('form_state', $formState);
      return $checkout;
    }
    return NULL;
  }

}
