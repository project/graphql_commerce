<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the order total summary.
 *
 * @DataProducer(
 *   id = "commerce_order_total_summary",
 *   name = @Translation("Commerce: Order Total Summary"),
 *   description = @Translation("Produces the order total summary."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The order total summary.")
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *   }
 * )
 */
class OrderTotalSummary extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The order total summary service.
   *
   * @var \Drupal\commerce_order\OrderTotalSummary
   */
  protected OrderTotalSummaryInterface $orderTotalSummary;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_order.order_total_summary')
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_order\OrderTotalSummary $orderTotalSummary
   *   The order total summary service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    OrderTotalSummaryInterface $orderTotalSummary
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->orderTotalSummary = $orderTotalSummary;
  }

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return array
   *   The order total summary.
   */
  public function resolve(OrderInterface $order, FieldContext $fieldContext) {
    return [...$this->orderTotalSummary->buildTotals($order), 'order' => $order];
  }

}
