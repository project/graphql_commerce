<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;

/**
 * The remove from cart producer.
 *
 * @DataProducer(
 *   id = "commerce_empty_cart",
 *   name = @Translation("Commerce: Empty Cart"),
 *   description = @Translation("Empty all order items from the cart."),
 *   produces = @ContextDefinition("entity:commerce_order",
 *     label = @Translation("The commerce cart."),
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *   }
 * )
 */
class EmptyCart extends CommerceCartDataProducerBase {

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order.
   *
   * @return \Drupal\graphql_commerce\Wrappers\CommerceMutationResult
   *   The result.
   */
  public function resolve(OrderInterface $order) {
    $result = new CommerceMutationResult($order);
    return $result;
  }

}
