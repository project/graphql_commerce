<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\Buffers\SubRequestBuffer;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Represents a checkout process for an order.
 *
 * @package Drupal\commerce_checkout
 */
class Checkout extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The cart session.
   *
   * @var \Drupal\commerce_cart\CartSessionInterface
   */
  protected $cartSession;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The subrequest buffer.
   *
   * @var \Drupal\graphql\GraphQL\Buffers\SubRequestBuffer
   */
  protected $subRequestBuffer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('form_builder'),
      $container->get('commerce_cart.cart_session'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('graphql.buffer.subrequest'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\commerce_cart\CartSessionInterface $cart_session
   *   The cart session.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart session.
   * @param \Drupal\graphql\GraphQL\Buffers\SubRequestBuffer $subRequestBuffer
   *   The subrequest buffer.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    CheckoutOrderManagerInterface $checkout_order_manager,
    FormBuilderInterface $form_builder,
    CartSessionInterface $cart_session,
    CartProviderInterface $cart_provider,
    SubRequestBuffer $subRequestBuffer,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->formBuilder = $form_builder;
    $this->cartSession = $cart_session;
    $this->cartProvider = $cart_provider;
    $this->subRequestBuffer = $subRequestBuffer;
  }

  public function resolve(OrderInterface $order, string $requestedStep = NULL, FieldContext $fieldContext) {
    $url = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
    ]);

    $resolve = $this->subRequestBuffer->add($url, function () use ($order, $requestedStep, $fieldContext) {
      $stepId = $this->checkoutOrderManager->getCheckoutStepId($order, $requestedStep);
      $checkoutFlow = $this->checkoutOrderManager->getCheckoutFlow($order);
      $checkoutFlowPlugin = $checkoutFlow->getPlugin();
      // If ($checkoutFlowPlugin instanceof MultistepDefault) {
      // $panes = $checkoutFlowPlugin->getPanes();
      // }
      // $steps = $checkoutFlowPlugin->getVisibleSteps();
      $fieldContext->setContextValue('commerce_checkout_step', $stepId);
      // $form = $this->formBuilder->getForm($checkoutFlowPlugin, $stepId);
      return $checkoutFlowPlugin;
    });

    return new Deferred(function () use ($resolve) {
      $response = $resolve();
      return $response;
    });
    // If ($requested_step_id != $step_id) {
    // $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id(), 'step' => $step_id]);
    // return new RedirectResponse($url->toString());
    // }
  }

}
