<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;

/**
 * Computed availability of a given variation.
 *
 * This will not consider stock availability unless an availability checker is
 * implemented that may return an unavailable result if the item is out of stock.
 * The default StockAvailabilityChecker provided with commerce_stock_enforcement
 * always returns a neutral result. This module relies on form validation to
 * prevent purchase of out of stock variations.
 *
 * @DataProducer(
 *   id = "commerce_computed_availability",
 *   name = @Translation("Commerce: Computed Availability"),
 *   description = @Translation("Computed availability of a variation."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The query result.")
 *   ),
 *   consumes = {
 *     "productVariation" = @ContextDefinition("entity:commerce_product_variation",
 *       label = @Translation("The product variation for the availability computation."),
 *     ),
 *    "quantity" = @ContextDefinition("any",
 *       required = FALSE,
 *       label = @Translation("The quantity for the the availability computation."),
 *     )
 *   }
 * )
 */
class ComputedAvailability extends CommerceCartDataProducerBase {

  /**
   * Resolves the availability of a product variation.
   *
   * @param ProductVariationInterface $entity
   *   The product variation entity to resolve availability for.
   * @param int|null $quantity
   *   The quantity of the product variation. Defaults to 1 if not provided.
   * @param FieldContext $field
   *   The field context to add cacheable dependency to.
   *
   * @return bool
   *   Whether the product variation is available or not.
   */
  public function resolve(ProductVariationInterface $entity, int|null $quantity, FieldContext $field) {
    $quantity = $quantity ?? 1;

    assert($entity instanceof ProductVariationInterface);
    $context = new RenderContext();
    $result = $this->renderer->executeInRenderContext($context, function () use ($entity, $quantity) {
      $orderItem = $this->cartManager->createOrderItem($entity);
      $orderItem->setQuantity($quantity);
      $availabilityContext = $this->getAvailabilityContext($entity);
      return $this->availabilityManager->check($orderItem, $availabilityContext);
    });
    $field->addCacheableDependency($context);
    return $result;

  }

}
