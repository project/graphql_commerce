<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_product\ProductVariationAttributeMapperInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the PreparedAttribute values.
 *
 * @DataProducer(
 *   id = "commerce_prepared_attribute_values",
 *   name = @Translation("Commerce: Product Attribute Value"),
 *   description = @Translation("Produces the CommercePreparedAttributeValue object."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The prepared attribute value.")
 *   ),
 *   consumes = {
 *     "preparedAttribute" = @ContextDefinition("any",
 *       label = @Translation("The prepared attribute object."),
 *     ),
 *   }
 * )
 */
class ProductAttributeValues extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The commerce product attribute mapper.
   *
   * @var \Drupal\commerce_product\ProductVariationAttributeMapper
   */
  protected $attributeMapper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_product.variation_attribute_mapper')
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_product\ProductVariationAttributeMapper $attributeMapper
   *   The product variation attribute mapper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    ProductVariationAttributeMapperInterface $attributeMapper
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->attributeMapper = $attributeMapper;
  }

  /**
   * The resolver.
   *
   * @param array $preparedAttribute
   *   The prepared attribute array.
   *
   * @return array
   *   The product attribute values.
   */
  public function resolve(array $preparedAttribute) {
    $values = $preparedAttribute['values'];
    $result = [];

    foreach ($values as $id => $label) {
      if ($id === '_none') {
        continue;
      }
      $result[] = [
        'id' => $id,
        'label' => $label,
        'attribute' => $preparedAttribute,
      ];
    }

    return $result;
  }

}
