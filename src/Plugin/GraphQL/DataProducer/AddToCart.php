<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;

/**
 * The add to cart producer.
 *
 * @DataProducer(
 *   id = "commerce_add_to_cart",
 *   name = @Translation("Commerce: Add to Cart"),
 *   description = @Translation("Add a product to the cart."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation result.")
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *     "id" = @ContextDefinition("any",
 *       label = @Translation("The product variation ID to add to the cart."),
 *     ),
 *    "quantity" = @ContextDefinition("any",
 *       label = @Translation("The quantity to add to the cart."),
 *     )
 *   }
 * )
 */
class AddToCart extends CommerceCartDataProducerBase {

  /**
   * Resolves a mutation on an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to resolve the mutation on.
   * @param mixed $id
   *   The ID of the product variation.
   * @param int $quantity
   *   The quantity of the product variation to add to the order.
   * @param FieldContext $field
   *   The field context.
   *
   * @return \Drupal\commerce_order\CommerceMutationResult
   *   The result of the mutation.
   */
  public function resolve(OrderInterface $order, $id, $quantity, FieldContext $field) {
    $result = new CommerceMutationResult($order);

    $storage = $this->entityTypeManager->getStorage('commerce_product_variation');
    /** @var \Drupal\commerce\PurchasableEntityInterface $entity */
    $entity = $storage->load($id);
    if (!$entity) {
      return $result->addProductVariationNotFoundError();
    }

    $context = new RenderContext();

    $this->renderer->executeInRenderContext($context, function () use ($order, $entity, $quantity, $result) {
      // Load the 'combine' setting from the AddToCart formatter on the view
      // display for the bundle.
      /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository */
      $entityDisplayRepository = \Drupal::service('entity_display.repository');
      $bundle = $entity->getProduct()?->bundle();
      $combine = TRUE;
      if ($bundle) {
        $viewDisplay = $entityDisplayRepository->getViewDisplay('commerce_product', $bundle);
        $combine = $viewDisplay->get('content')['variations']['settings']['combine'] ?? TRUE;
      }
      if (!$this->addStockViolationsToResult($entity, $quantity, $result)) {
        $this->cartManager->addEntity($order, $entity, $quantity, $combine);
      }

    });

    $field->addCacheableDependency($context);
    return $result;
  }

}
