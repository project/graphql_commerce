<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;
use Drupal\graphql_commerce\Wrapper\CommerceMutationResult;

/**
 * The remove from cart producer.
 *
 * @DataProducer(
 *   id = "commerce_remove_from_cart",
 *   name = @Translation("Commerce: Remove from Cart"),
 *   description = @Translation("Removes an order item from the cart."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation result."),
 *   ),
 *   consumes = {
 *     "order" = @ContextDefinition("entity:commerce_order",
 *       label = @Translation("The commerce order."),
 *     ),
 *     "id" = @ContextDefinition("any",
 *       label = @Translation("The order item id to remove."),
 *     )
 *   }
 * )
 */
class RemoveFromCart extends CommerceCartDataProducerBase {

  /**
   * Resolves the given order item ID and removes it from the order.
   *
   * @param \Drupal\commerce_order\OrderInterface $order
   *   The order object for which to remove the order item.
   * @param int $id
   *   The ID of the order item to be removed.
   * @param \Drupal\Core\Field\FieldContext $field
   *   The field context object.
   *
   * @return \Drupal\commerce_cart\CommerceMutationResult
   *   The result of the mutation operation.
   */
  public function resolve(OrderInterface $order, $id, FieldContext $field) {
    $result = new CommerceMutationResult($order);

    $storage = $this->entityTypeManager->getStorage('commerce_order_item');
    $entity = $storage->load($id);
    if (!$entity) {
      return $result->addProductVariationNotFoundError();
    }

    $context = new RenderContext();

    $this->renderer->executeInRenderContext($context, function () use ($order, $entity) {
      $this->cartManager->removeOrderItem($order, $entity);
    });

    $field->addCacheableDependency($context);
    return $result;
  }

}
