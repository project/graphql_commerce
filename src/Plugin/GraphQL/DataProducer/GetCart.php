<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_commerce\Plugin\GraphQL\CommerceCartDataProducerBase;

/**
 * The remove from cart producer.
 *
 * @DataProducer(
 *   id = "commerce_get_cart",
 *   name = @Translation("Commerce: Get Cart"),
 *   description = @Translation("Get the cart for the current session."),
 *   produces = @ContextDefinition("entity:commerce_order",
 *     label = @Translation("The commerce cart."),
 *   ),
 *   consumes = {
 *     "create" = @ContextDefinition("boolean",
 *       label = @Translation("If a new cart should be created."),
 *     ),
 *   }
 * )
 */
class GetCart extends CommerceCartDataProducerBase {

  /**
   * Resolves the cart for the current session.
   *
   * @param bool $create
   *   Whether a new cart should be created if none exists.
   * @param FieldContext $context
   *   The field context.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The cart for the current session.
   */
  public function resolve(bool $create, FieldContext $context) {
    $cart = $this->cartProvider->getCart('default');

    // Create a cart if requested.
    if (!$cart && $create) {
      $cart = $this->cartProvider->createCart('default');
    }

    // Add the session to the cache context since a cart always needs a session.
    $context->addCacheContexts(['session']);
    $context->addCacheableDependency($cart);
    return $cart;
  }

}
