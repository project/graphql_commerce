<?php

namespace Drupal\graphql_commerce\Plugin\GraphQL\DataProducer;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product\ProductVariationAttributeMapperInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The add to cart producer.
 *
 * @DataProducer(
 *   id = "commerce_product_attributes",
 *   name = @Translation("Commerce: Product Attributes"),
 *   description = @Translation("Produces a list of product attribute values."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The product attributes.")
 *   ),
 *   consumes = {
 *     "variation" = @ContextDefinition("entity:commerce_product_variation",
 *       label = @Translation("The commerce product variation."),
 *     ),
 *   }
 * )
 */
class ProductAttributes extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The commerce product attribute mapper.
   *
   * @var \Drupal\commerce_product\ProductVariationAttributeMapper
   */
  protected $attributeMapper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('commerce_product.variation_attribute_mapper')
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\commerce_product\ProductVariationAttributeMapper $attributeMapper
   *   The product variation attribute mapper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    ProductVariationAttributeMapperInterface $attributeMapper
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->attributeMapper = $attributeMapper;
  }

  /**
   * The resolver.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $variation
   *   The product variation.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return \Drupal\commerce_product\Entity\ProductAttributeValue[]
   *   The product attributes.
   */
  public function resolve(ProductVariationInterface $variation, FieldContext $fieldContext) {
    $product = $variation->getProduct();
    $variations = $product->getVariations();
    $attributes = $this->attributeMapper->prepareAttributes($variation, $variations);
    $fieldContext->setContextValue('commerce_product_variation', $variation);

    $result = [];

    foreach ($attributes as $fieldName => $attribute) {
      $item = $attribute->toArray();
      $item['field_name'] = $fieldName;
      $result[] = $item;
    }

    return $result;
  }

}
