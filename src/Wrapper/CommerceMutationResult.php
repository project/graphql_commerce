<?php

declare(strict_types=1);

namespace Drupal\graphql_commerce\Wrapper;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Helper class that contains commerce mutation results.
 */
class CommerceMutationResult {

  use StringTranslationTrait;

  /**
   * Errors.
   *
   * @var string[]
   */
  protected array $errors = [];

  /**
   * QueryConnection constructor.
   *
   * @param \Drupal\commerce_order\Entity\Order|null $order
   *   The commerce order.
   */
  public function __construct(
    protected ?OrderInterface $order = NULL
  ) {
  }

  /**
   * Get the order.
   *
   * @return \Drupal\commerce_order\Entity\Order|null
   *   The order.
   */
  public function getOrder(): ?OrderInterface {
    return $this->order;
  }

  /**
   * Add an error.
   *
   * @param string $error
   *   The error message.
   */
  public function addError(string $error): static {
    $this->errors[] = $error;
    return $this;
  }

  /**
   * Add the product variation not found error.
   */
  public function addProductVariationNotFoundError(): static {
    $this->errors[] = (string) $this->t('Product variation not found.');
    return $this;
  }

  /**
   * Returns the errors.
   *
   * @return string[]
   *   The errors.
   */
  public function getErrors(): array {
    return $this->errors;
  }

  /**
   * Returns TRUE if the mutation was successful.
   *
   * @return bool
   *   If the mutation was successful.
   */
  public function isSuccess(): bool {
    return empty($this->errors);
  }

}
